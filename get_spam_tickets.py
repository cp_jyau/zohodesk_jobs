# Pull all old tickets from Zoho and load all the non migration ones to Redshift
# This gives a good starting point for the API.

import sys
import boto3
import glob
import logging
import boto
import os
import requests
import json
import uuid
import csv
import pyodbc
from json.decoder import JSONDecodeError


DELETE_FILES = True

logging.basicConfig(level=logging.DEBUG)

#BATCHES=500 # Number of Zoho batches of 100 to retrieve
#START_INDEX=200000
ZOHO_CLIENT_ID='1000.KQ3DRCVBBN30NZ2DT3JR3KGE3S3ONI'
ZOHO_CLIENT_SECRET='02429929a73ad1e4d95112c1a0320c8f7450e75e01'
BATCHES=15
START_INDEX=1
AUTHTOKEN = None
STORAGE_PATH = "data/spam"
TICKET_FIELDS = [ 
        'modifiedTime',
        'ticketNumber',
        'id'
]

def retrieve_from_zoho(start):
    """ Get 100 tickets from Zoho starting from given index and write them to files as JSON"""
    url = "https://desk.zoho.com/api/v1/tickets"
    payload = {"from": start, "limit": "100", "sortBy" : "-ticketNumber", "viewId" : "488342000000007531", 'fields': 'modifiedTime,ticketNumber'} 
    
    headers = {
            "Authorization": "Zoho-oauthtoken %s" % AUTHTOKEN,
            "orgId": "712198972"
            }
    r = requests.get(url, payload, headers=headers)
    try:
        data = r.json()['data']
    except JSONDecodeError:
        logging.info('No data found - that seems odd. Maybe someone just emptied the Spam folder?') 
        return
    except KeyError:
        logging.info('No data found - KeyError - maybe no more tickets?')
        return
    
    myuuid = uuid.uuid1()
    file_path = "{0}/json/".format(STORAGE_PATH)
    os.makedirs(file_path, exist_ok=True)
    filename = "{0}{1}".format(file_path, myuuid)
    with open(filename, "w") as outfile:
        logging.info("Writing to %s" % myuuid)
        json.dump(r.json(), outfile)

def get_some_tickets():
    iterations=BATCHES
    myindex = START_INDEX
    for i in range(iterations):
        logging.info("Retrieving 100 tickets starting at index {0}".format(myindex))        
        retrieve_from_zoho(myindex)
        myindex += 100

def json_to_csv(filename, ticket_writer):
    """ Read JSON ticket file and write it to csv file
        filename: input JSON file to read from
        dictWriter: output csv.DictWriter object to write to
        """
    logging.info("Appending JSON tickets to CSV")
    with open(filename, 'r') as infile:
        data = json.load(infile)
        for ticket in data['data']:
            if 'ticketNumber' in ticket:
                # write the ticket
                ticket_writer.writerow(ticket)
            else:
                logging.debug('No ticket data found. This must be just a custom fields file.')
            
def make_dict_writer(outfile, fields):
    return csv.DictWriter(
            outfile,
            fields,
            extrasaction='ignore',
            restval=None,
            delimiter=',', 
            quotechar='"', 
            escapechar='\\',
            quoting=csv.QUOTE_MINIMAL)


def process_available_tickets():
    logging.info("Begin processing available tickets")
    os.makedirs("{0}/csv/".format(STORAGE_PATH), exist_ok=True)
    ticket_filename = "{0}/csv/tickets.csv".format(STORAGE_PATH)
    if os.path.exists(ticket_filename):
        append_write = 'a'
    else:
        append_write = 'w'
    with open(ticket_filename, append_write) as ticketfile:
        ticket_writer = make_dict_writer(ticketfile, TICKET_FIELDS)
        for filename in glob.glob("{0}/json/*".format(STORAGE_PATH)):
            json_to_csv(filename, ticket_writer)
            if DELETE_FILES:
                os.remove(filename)

def upload_to_s3(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

def redshift_load_tickets(cursor):
    """ Load file from S3 into staging """
    logging.info("Load data from S3 to Redshift")
    cursor.execute("truncate zoho_staging.spam_tickets")

    sql = """
    copy zoho_staging.spam_tickets from 's3://zohodesk-redshift/spam_tickets.csv'
    iam_role 'arn:aws:iam::299457626271:role/RedshiftCopyUnload'
    delimiter ','
    NULL as 'NULL'
    ignoreblanklines
    timeformat 'YYYY-MM-DDTHH:MI:SS'
    CSV
    TRUNCATECOLUMNS
    ACCEPTINVCHARS
    ;"""
    cursor.execute(sql)

def redshift_update_tickets(cursor):
    """ Update the tickets table, setting spam flag """
    sql = """update zoho.tickets set is_spam=1
    from zoho_staging.spam_tickets
    where zoho.tickets.ticket_id = zoho_staging.spam_tickets.ticket_id
    """
    cursor.execute(sql)

def send_tickets_to_redshift():
    """ Upload tickets.csv file to S3 and load to Redshift """
    uid = os.environ['REDSHIFT_USER']
    pwd = os.environ['REDSHIFT_PASSWORD']

    db_conn_string = 'Driver=/opt/amazon/redshiftodbc/lib/64/libamazonredshiftodbc64.so;UID=%s;PWD=%s;Database=%s;Port=%s;Server=%s' % (uid, pwd, 'transfor', 5439, '10.200.3.36')
    conn = pyodbc.connect(db_conn_string)
    cursor = conn.cursor()

    filename = "{0}/csv/tickets.csv".format(STORAGE_PATH)
    upload_to_s3(
	file_name=filename,
	bucket='zohodesk-redshift',
	object_name="spam_tickets.csv")
    redshift_load_tickets(cursor)
    os.remove(filename)
    redshift_update_tickets(cursor)

def get_access_token():
    """ Use Zoho Oauth to get an access token """
    zoho_oauth_url = "https://accounts.zoho.com/oauth/v2/token"
    zoho_oauth_headers = {"orgId": "712198972"}

    try:
        refresh_token_file = 'refresh_token.txt'
        with open(refresh_token_file, 'r') as infile:
            refresh_token = infile.read().strip()
            logging.debug("retrieved existing refresh token from filesystem:", refresh_token)

        # use refresh token to get a fresh access token
        payload = {"refresh_token": refresh_token, "client_id": ZOHO_CLIENT_ID, "client_secret": ZOHO_CLIENT_SECRET, "grant_type": "refresh_token"}
        r = requests.post(zoho_oauth_url, payload, headers=zoho_oauth_headers)
        res = r.json()
        logging.debug(res)
        return res['access_token']

    except FileNotFoundError:
        print("No refresh token found. Let's try and get a new one.")
        print("Generate a grant token on the Zoho API console with scope Desk.tickets.READ and enter it here.")
        grant_token=input().strip()

        # request a refresh and access token

        payload = {"code": grant_token, "grant_type": "authorization_code", "client_id": ZOHO_CLIENT_ID, "client_secret": ZOHO_CLIENT_SECRET}
        headers = {"orgId": "712198972"}        
        r = requests.post(zoho_oauth_url, payload, headers=zoho_oauth_headers)
        res = r.json()
        logging.debug(res)

        # store the refresh token
        with open(refresh_token_file, 'w') as outfile:
            outfile.write(res['refresh_token'])
        logging.debug('Returning access token %s' % res['access_token'])
        return res['access_token']



def main():    
    global AUTHTOKEN
    AUTHTOKEN = get_access_token()
    logging.debug("Successfully retrieved an access token: {}".format(AUTHTOKEN))

    if len(sys.argv) != 2:
        print("Usage: python _tickets.py [get | process | load]")
        exit()

    if sys.argv[1] == 'get':
        get_some_tickets()
    if sys.argv[1] == 'process':
        process_available_tickets()
    elif sys.argv[1] == 'load':
        send_tickets_to_redshift()
    elif sys.argv[1] == 'run':
        get_some_tickets()
        process_available_tickets()
        send_tickets_to_redshift()

if __name__ == '__main__':
    main()
