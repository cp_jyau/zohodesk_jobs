1. Set up a virtualenv environment
2. Update run_get_spam to refer to new virtualenv environment
3. Install required modules
pip install -r requirements.txt
4. Then run script without any arguments to trigger Oauth token retrieval
5. Schedule run_get_spam with a job scheduler

